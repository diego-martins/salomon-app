package com.onpister.salomon

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import com.google.android.gms.ads.AdRequest
import java.math.BigDecimal
import java.text.NumberFormat
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.view.View
import android.widget.Toast
import android.content.ClipData
import android.content.ClipboardManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability


class Utils
{
    object Ad{

        fun adRequest(): AdRequest {
            val builder = AdRequest.Builder()
            if (BuildConfig.DEBUG) {
                builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                builder.addTestDevice("")
                builder.addTestDevice("")
                builder.addTestDevice("")
            }
            return builder.build()
        }

    }

    object Text{

        fun formattStringWithoutSymbol(str: String): String {
            var res: String = if(!str.isNullOrEmpty()) str.replace(Regex("(\\D+|!\\.,)"), "") else "0"
            res = res.padStart(3, '0')
            val nf = NumberFormat.getCurrencyInstance()
            try{
                var value: Double = res.toDouble()
                var nstr = nf.format(value/100)
                return nstr.replace(nf.currency.symbol, "")
            }catch (e:NumberFormatException){
            }
            return str;
        }

        fun formattString(str: String): String {
            var res: String = if(!str.isNullOrEmpty()) str.replace(Regex("(\\D+|!\\.,)"), "") else "0"
            res = res.padStart(3, '0')
            val nf = NumberFormat.getCurrencyInstance()
            try{
                var value: Double = res.toDouble()
                return nf.format(value/100)
            }catch (e:NumberFormatException){
            }
            return str;
        }

        fun formattString(value: Double): String {
            var str: String = BigDecimal(value).setScale(2, BigDecimal.ROUND_HALF_UP).toDouble().toString()
            var res: String = if(!str.isNullOrEmpty()) str.replace(Regex("(\\D+|!\\.,)"), "") else "0"
            res = res.padEnd(3, '0')
            val nf = NumberFormat.getCurrencyInstance()
            try{
                var _value: Double = res.toDouble()
                return nf.format(_value/100)
            }catch (e:NumberFormatException){
            }
            return str;
        }

        fun formattDecimal(str: String): Double {
            var res: String = if(!str.isNullOrEmpty()) str.replace(Regex("\\D+"), "") else "0"
            var value: Double = res.toDouble()/100
            return value
        }

        @SuppressWarnings("deprecation")
        fun toHtml(str: String): Spanned {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                return Html.fromHtml(str, Html.FROM_HTML_MODE_LEGACY)
            }else{
                return Html.fromHtml(str)
            }
        }
    }

    object Network {
        fun isOnline(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnectedOrConnecting
        }
    }

    object Media {

        fun copyToClipboard(context: Context, text: String) {
            copyToClipboard(context, text, context.getString(R.string.msg_copied))
        }

        fun copyToClipboard(context: Context, text: String, message: String) {
            try {
                val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clip = ClipData.newPlainText(context.getString(R.string.app_name), text.replace("\\*".toRegex(), ""))
                clipboard.setPrimaryClip(clip)
                if (!message.isNullOrEmpty()) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        fun openWeb(context: Context, url: String) {
            val intentWeb = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            context.startActivity(intentWeb)
        }

        fun getShareIntent(context: Context, text: String): Intent {
            return getShareIntent(context, "", text)
        }

        fun getShareIntent(context: Context, title: String, text: String): Intent {
            var _title = title
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"

            if (!_title.isNullOrEmpty()) {
                intent.putExtra(Intent.EXTRA_TITLE, _title)
            }

            intent.putExtra(Intent.EXTRA_TEXT, text)
            return Intent.createChooser(intent, context.getString(R.string.label_share_via))
        }

        fun getShareIntent(context: Context, title: String, text: String, uri: Uri): Intent {

            var _title = title
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = context.contentResolver.getType(uri)

            if (!_title.isNullOrEmpty()) {
                intent.putExtra(Intent.EXTRA_TITLE, _title)
            }

            intent.putExtra(Intent.EXTRA_EMAIL, "")
            intent.putExtra(Intent.EXTRA_TEXT, text)
            intent.putExtra(Intent.EXTRA_STREAM, uri)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

            //intent.setDataAndType(uri, context.contentResolver.getType(uri))
            return Intent.createChooser(intent, context.getString(R.string.label_share_via))
        }
    }

    object Image {
        fun viewToImage(view: View): Bitmap{
            val returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(returnedBitmap)
            val bgDrawable = view.getBackground()

            if (bgDrawable != null) {
                bgDrawable.draw(canvas)
            }
            else {
                canvas.drawColor(Color.WHITE)
            }

            view.draw(canvas)
            return returnedBitmap
        }
    }

    object Package {
        fun checkPlayService(context: Activity, showDialog: Boolean): Boolean{
            var googleAPI: GoogleApiAvailability = GoogleApiAvailability.getInstance()
            var result = googleAPI.isGooglePlayServicesAvailable(context)
            if(result != ConnectionResult.SUCCESS){
                var reqPlayServiceCode = 1
                if(showDialog && googleAPI.isUserResolvableError(result)){
                    googleAPI.getErrorDialog(context, result, reqPlayServiceCode).show()
                }
                return false
            }
            return true
        }
    }
}
