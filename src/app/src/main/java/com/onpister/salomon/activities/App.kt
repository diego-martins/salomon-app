package com.onpister.salomon.activities

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.crashlytics.android.Crashlytics
import com.google.android.gms.ads.MobileAds
import com.onpister.salomon.BuildConfig
import io.fabric.sdk.android.Fabric

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        if(!BuildConfig.DEBUG) {
            val fabric = Fabric.Builder(this)
                    .kits(Crashlytics())
                    .debuggable(true)
                    .build()
            if (!io.fabric.sdk.android.BuildConfig.DEBUG && fabric != null) Fabric.with(fabric)
        }

        MobileAds.initialize(this);

        //if (!FirebaseApp.getApps(this).isEmpty()) {
        //    FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        //}
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}