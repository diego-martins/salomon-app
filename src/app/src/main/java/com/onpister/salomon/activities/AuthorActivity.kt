package com.onpister.salomon.activities

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.method.LinkMovementMethod
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.FirebaseFirestore

import com.onpister.salomon.R
import com.onpister.salomon.Utils
import com.onpister.salomon.models.AuthorModel
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_author.*
import kotlinx.android.synthetic.main.content_author.*

class AuthorActivity : AppCompatActivity() {

    private var reqPermissionCode = 1
    private var firebaseAnalytics: FirebaseAnalytics? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_author)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        loadEvents()
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), reqPermissionCode)
        }else{
            loadData()
        }
    }

    private fun loadData(){

        var id = intent.extras.getString("AUTHOR_ID")
        var db = FirebaseFirestore.getInstance()
        db.collection("authors").document(id).get()
                .addOnCompleteListener {
                    task ->
                    loadingPanel.visibility = View.INVISIBLE
                    if(task.isSuccessful){
                        var result = task.result.toObject(AuthorModel::class.java)
                        var lang = getString(R.string.pref_lang_default)
                        var _title = ""
                        var _about = ""
                        if(lang.equals("pt")){
                            _title = if(result.namePt.isNotEmpty()) result.namePt else result.nameEn
                            _about = if(result.aboutPt.isNotEmpty()) result.aboutPt else result.aboutEn
                        }
                        else if(lang.equals("en")){
                            _title = if(result.nameEs.isNotEmpty()) result.nameEs else result.nameEn
                            _about = if(result.aboutEs.isNotEmpty()) result.aboutEs else result.aboutEn
                        }
                        else {
                            _title = result.nameEn
                            _about = result.aboutEn
                        }
                        supportActionBar!!.title = _title
                        about.text = Utils.Text.toHtml(_about)

                        if(result.imageUrl.isNotEmpty()){

                            Picasso
                                    .with(this@AuthorActivity)
                                    .load(result.imageUrl)
                                    .placeholder(R.drawable.img_author_default)
                                    .error(R.drawable.img_author_default)
                                    .networkPolicy(NetworkPolicy.OFFLINE)
                                    .into(img_author, object: Callback{
                                        override fun onSuccess() {
                                        }
                                        override fun onError() {
                                            Picasso
                                                    .with(this@AuthorActivity)
                                                    .load(result.imageUrl)
                                                    .placeholder(R.drawable.img_author_default)
                                                    .error(R.drawable.img_author_default)
                                                    .into(img_author)
                                        }
                                    })

                        }
                    }else{
                        finish()
                    }
                }
                .addOnFailureListener {
                    exception ->
                    exception.printStackTrace()
                    loadingPanel.visibility = View.INVISIBLE
                    finish()
                }
    }

    private fun loadEvents(){
        about.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> {
                loadingPanel.visibility = View.INVISIBLE
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            reqPermissionCode -> {
                if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    loadData()
                }else{
                    loadData()
                }
            }
        }
    }
}
