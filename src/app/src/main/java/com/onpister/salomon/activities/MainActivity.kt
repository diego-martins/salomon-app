package com.onpister.salomon.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.LinearLayout
import android.widget.Toast
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.NativeExpressAdView
import com.google.firebase.firestore.*
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.onpister.salomon.*
import com.onpister.salomon.R
import com.onpister.salomon.adapters.MyAdapter
import com.onpister.salomon.adapters.MyInfiniteScrollListener
import com.onpister.salomon.models.PhraseModel
import com.onpister.salomon.models.UserPrefs
import kotlinx.android.synthetic.main.action_bar.*

import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileOutputStream
import java.util.*
import kotlin.collections.ArrayList
import android.support.v4.content.ContextCompat
import com.google.firebase.analytics.FirebaseAnalytics

class MainActivity : AppCompatActivity() {

    private val reqSETTINGS = 1
    private var reqAUTHOR = 2
    private var reqPermissionCode = 3

    var firstAt: Long = -1L
    var lastAt: Long = -1L
    var currentAt: Long = -1L
    var num: Long = 10L
    var loading = false
    var currentImgIndex = 0
    var clearBefore: Boolean = false

    private var firebaseAnalytics: FirebaseAnalytics? = null

    var mAdapter: MyAdapter? = null
    private var userPrefs: UserPrefs? = null
    private lateinit var layoutManager: LinearLayoutManager
    private var db = FirebaseFirestore.getInstance()

    private var imgs: IntArray = intArrayOf(
            R.drawable.img_phrase_1,
            R.drawable.img_phrase_2,
            R.drawable.img_phrase_3,
            R.drawable.img_phrase_4,
            R.drawable.img_phrase_5,
            R.drawable.img_phrase_6,
            R.drawable.img_phrase_7,
            R.drawable.img_phrase_8,
            R.drawable.img_phrase_9,
            R.drawable.img_phrase_10,
            R.drawable.img_phrase_11,
            R.drawable.img_phrase_12,
            R.drawable.img_phrase_13,
            R.drawable.img_phrase_14,
            R.drawable.img_phrase_15
    )

    private fun ClosedRange<Int>.random() = Random().nextInt(endInclusive - start) + start

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        //toolbar.setLogo(R.drawable.ic_face_white)
        //title = ""

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        userPrefs = UserPrefs(this)
        mAdapter = MyAdapter(this)
        listView.adapter = mAdapter
        //layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false )
        layoutManager = LinearLayoutManager(this )
        listView.layoutManager = layoutManager

        var size = imgs.size - 1
        for(index in 0..size){
            var tmp = imgs[index]
            var nIndex = (0..size).random()
            imgs[index] = imgs[nIndex]
            imgs[nIndex] = tmp
        }

        addEvents()
        loadAd()
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), reqPermissionCode)
        }else{
            loadData()
        }

    }

    private fun addEvents(){

        pullRefresh.setOnRefreshListener {
            loadLastData()
        }

        mAdapter?.setOnCopyClick(object: MyAdapter.OnClickCopy{
            override fun onClick(model: PhraseModel) {
                var text = model.text + " - " + model.authorName + "\n\n" + getString(R.string.app_play_store)
                Utils.Media.copyToClipboard(this@MainActivity, text)
            }
        })

        mAdapter?.setOnShareClick(object: MyAdapter.OnClickShare{
            override fun onClick(phrase: PhraseModel, view: View) {
                shareIt(phrase, view)
            }
        })

        mAdapter?.setOnAuthorClick(object: MyAdapter.OnClickAuthor{
            override fun onClick(phrase: PhraseModel) {
                var intent = Intent(this@MainActivity, AuthorActivity::class.java)
                intent.putExtra("AUTHOR_ID", phrase.authorId)
                startActivityForResult(intent, reqAUTHOR)
            }
        })

        listView.addOnScrollListener(MyInfiniteScrollListener({loadData()}, layoutManager))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_share -> {
                val shareIntent = Utils.Media.getShareIntent(this, getString(R.string.app_play_store))
                startActivity(shareIntent)
                return true
            }
            R.id.action_rate -> {
                Utils.Media.openWeb(this, "market://details?id=" + packageName)
                return true
            }
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivityForResult(intent, reqSETTINGS)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun loadAd(){

        var remoteConfig = FirebaseRemoteConfig.getInstance()
        var adEnabled = remoteConfig.getBoolean("ad_enabled")
        var adType = remoteConfig.getString("ad_type")
        var adProvider = remoteConfig.getString("ad_provider")

        if(!adEnabled) return

        if(adProvider == "admob") {
            if(adType == "native"){
                var adView = NativeExpressAdView(this)
                adView.adUnitId = getString(R.string.ad_admob_native)
                adView.adSize = AdSize(AdSize.FULL_WIDTH, 80)
                adView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                adView.adListener = object : AdListener() {
                    override fun onAdLoaded() {
                        adViewContainer.removeAllViews()
                        adViewContainer.addView(adView)
                        super.onAdLoaded()
                    }
                }
                adView.loadAd(Utils.Ad.adRequest())
            }
            else if(adType == "banner"){
                var adView = AdView(this)
                adView.adUnitId = getString(R.string.ad_admob_banner)
                adView.adSize = AdSize.SMART_BANNER
                adView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                adView.adListener = object : AdListener() {
                    override fun onAdLoaded() {
                        adViewContainer.removeAllViews()
                        adViewContainer.addView(adView)
                        super.onAdLoaded()
                    }
                }
                adView.loadAd(Utils.Ad.adRequest())
            }
        }
    }

    private fun loadData(){

        if(loading) {
            pullRefresh.isRefreshing = false
            return
        }

        loading = true
        var query = db.collection("phrases")
                .whereEqualTo("enabled", true)
                .whereEqualTo("lang", getString(R.string.pref_lang_default))
                .orderBy("timestamp", Query.Direction.DESCENDING)

        var _clearBefore = clearBefore
        if(clearBefore) {
            clearBefore = false
            currentAt = -1
            currentImgIndex = 0
            userPrefs?.lastPhraseAt = -1
        }

        if(currentAt == -1L && userPrefs!!.lastPhraseAt >= 0L){
            currentAt = userPrefs!!.lastPhraseAt
            query = query.whereLessThanOrEqualTo("timestamp", currentAt)
        }
        else if(currentAt >= 0L){
            query = query.whereLessThan("timestamp", currentAt)
        }

        query.limit(num)
            .get()
            .addOnCompleteListener {
                task ->
                if(task.isSuccessful){
                    task.result as QuerySnapshot
                    var list: MutableList<Any> = ArrayList()
                    for(doc in task.result){
                        var item = doc.toObject(PhraseModel::class.java)
                        if(!item.text.isNullOrEmpty()) {
                            item.id = doc.id
                            item.img = imgs[currentImgIndex]
                            item.text = "\"" + item.text.trim().toUpperCase().replace("\"", "") + "\""
                            list.add(item)
                            currentAt = item.timestamp
                            if (lastAt == -1L) lastAt = currentAt
                            firstAt = currentAt
                            currentImgIndex++
                            if (currentImgIndex >= imgs.size) currentImgIndex = 0
                        }
                    }
                    if(_clearBefore) {
                        mAdapter?.clear()
                    }
                    mAdapter?.add(list)
                    if(list.size < num){
                        mAdapter?.removeLoading()
                    }
                }else{
                    mAdapter?.removeLoading()
                    println(task.exception)
                }
                loading = false
                pullRefresh.isRefreshing = false
            }
                .addOnFailureListener {
                    exception -> run {
                        exception.printStackTrace()
                        pullRefresh.isRefreshing = false
                    }
                }

    }

    private fun loadLastData(){
        clearBefore = true
        loadData()
    }

    fun shareIt(phrase: PhraseModel, view: View){
        view.findViewById<LinearLayout>(R.id.viewOptions).visibility = View.INVISIBLE
        view.findViewById<LinearLayout>(R.id.viewAppSignature).visibility = View.VISIBLE
        Handler().postDelayed({
            var bitmap = Utils.Image.viewToImage(view.findViewById<View>(R.id.cardView))
            view.findViewById<LinearLayout>(R.id.viewAppSignature).visibility = View.INVISIBLE
            view.findViewById<LinearLayout>(R.id.viewOptions).visibility = View.VISIBLE
            var file = File(cacheDir, phrase.id + ".png")
            try {
                var stream = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                stream.close()

                var contentUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, file)
                if(contentUri != null) {
                    startActivity(Utils.Media.getShareIntent(this, "", getString(R.string.app_play_store), contentUri))
                }

            }catch (e: Exception){
                e.printStackTrace()
                Toast.makeText(this, R.string.error_share, Toast.LENGTH_SHORT).show()
            }
        }, 100)
    }

    override fun onPause() {
        var position = layoutManager.findFirstVisibleItemPosition()
        if(position >= 0) {
            var item = mAdapter?.getItem(position)
            if (item is PhraseModel) {
                userPrefs!!.lastPhraseAt = item.timestamp
            }
        }
        super.onPause()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            reqPermissionCode -> {
                if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    loadData()
                }else{
                    loadData()
                }
            }
        }
    }
}
