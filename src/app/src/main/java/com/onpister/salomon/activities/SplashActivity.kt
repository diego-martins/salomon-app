package com.onpister.salomon.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.onpister.salomon.BuildConfig
import com.onpister.salomon.R
import com.onpister.salomon.models.UserPrefs
import com.onpister.salomon.Utils

class SplashActivity : AppCompatActivity() {

    private var firebaseAnalytics: FirebaseAnalytics? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        if(!Utils.Package.checkPlayService(this, true)){
            finish()
        }else {
            loadRemoteConfig()
        }
    }

    fun loadRemoteConfig(){

        if(!Utils.Network.isOnline(this)){
            target(true)
            return
        }

        var remoteConfig = FirebaseRemoteConfig.getInstance()
        var settings = FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build()
        remoteConfig.setConfigSettings(settings)
        remoteConfig.setDefaults(R.xml.remote_config_defaults)

        var cacheExpiration: Long = 3600 // 1 hour in seconds.
        if(remoteConfig.info.configSettings.isDeveloperModeEnabled){
            cacheExpiration = 0
        }

        remoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, OnCompleteListener { task ->
                    if (task.isSuccessful) {
                        remoteConfig.activateFetched()
                        println("remoteConfig fetch success!")
                    } else {
                        println("remoteConfig fetch failed!")
                    }
                })
                .addOnFailureListener(this, OnFailureListener { exception ->
                    println(exception.localizedMessage)
                    target(false)
                }).addOnSuccessListener { _ ->
            target(false)
        }
    }

    fun loadFCM(){
        val firebaseMessaging = FirebaseMessaging.getInstance()
        if (firebaseMessaging != null) {
            if(BuildConfig.DEBUG) {
                firebaseMessaging.subscribeToTopic("dev")
            }
            var userPrefs = UserPrefs(this)
            if(userPrefs.notification) {
                firebaseMessaging.subscribeToTopic("everyone")
                firebaseMessaging.subscribeToTopic(getString(R.string.pref_lang_default))
            }else{
                firebaseMessaging.unsubscribeFromTopic("everyone")
                firebaseMessaging.unsubscribeFromTopic("en")
                firebaseMessaging.unsubscribeFromTopic("pt")
                firebaseMessaging.unsubscribeFromTopic("es")
            }
            firebaseMessaging.subscribeToTopic("everyone")
        }
    }

    fun target(wait: Boolean){

        Handler().postDelayed({
            loadFCM()
        }, 500)

        if(wait){
            Handler().postDelayed({
                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            }, 300)
        }else{
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}
