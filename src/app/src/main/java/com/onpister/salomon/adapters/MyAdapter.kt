package com.onpister.salomon.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.ads.AdView
import com.onpister.salomon.*
import com.onpister.salomon.adapters.holders.AdHolder
import com.onpister.salomon.adapters.holders.LoadingHolder
import com.onpister.salomon.adapters.holders.PhraseHolder
import com.onpister.salomon.models.AdModel
import com.onpister.salomon.models.LoadingModel
import com.onpister.salomon.models.PhraseModel

class MyAdapter(private val context: Context) : Adapter<RecyclerView.ViewHolder>() {

    private val typePHRASE = 1
    private val typeAD = 2
    private val typeLOADING = 3

    private var mList: MutableList<Any> = ArrayList()
    private var mClickCopyListener: OnClickCopy? = null
    private var mClickShareListener: OnClickShare? = null
    private var mClickAuthorListener: OnClickAuthor? = null

    init {
        add(LoadingModel())
    }

    fun clear(){
        mList.clear()
    }

    fun add(position: Int, model: Any){
        mList.add(position, model)
        notifyItemInserted(position)
    }

    fun add(model: Any){
        mList.add(model)
    }

    fun add(models: MutableList<Any>){
        //removeLoading()
        if(mList.isNotEmpty()) {
            mList.addAll(mList.size - 1, models)
        }else{
            removeLoading()
            mList.addAll(models)
            add(LoadingModel())
        }
        //add(LoadingModel())
        notifyItemChanged(0, mList.size + 1)
    }

    fun removeLoading(){
        if(mList.isNotEmpty() && mList.last() is LoadingModel){
            var position = mList.size - 1
            mList.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun setOnCopyClick(listener: OnClickCopy?){
        mClickCopyListener = listener
    }

    fun setOnShareClick(listener: OnClickShare?){
        mClickShareListener = listener
    }

    fun setOnAuthorClick(listener: OnClickAuthor?){
        mClickAuthorListener = listener
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        var itemType = getItemViewType(position)
        if(itemType == typePHRASE) {
            val model = mList.get(position)
            holder as PhraseHolder
            holder.bindView(model as PhraseModel)
            holder.bindEvent(model, mClickCopyListener, mClickShareListener, mClickAuthorListener)
        }
        else if(itemType == typeAD){
            holder as AdHolder
            holder.bindView(AdView(context), context.getString(R.string.ad_admob_banner))
        }
    }

    override fun getItemViewType(position: Int): Int =
        when(mList[position]){
            is PhraseModel -> typePHRASE
            is LoadingModel -> typeLOADING
            is AdModel -> typeAD
            else -> typePHRASE
        }

    override fun getItemCount(): Int = mList.size

    fun getItem(position: Int): Any?{
        if(position < mList.size) {
            return mList[position]
        }
        return null
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder =
        when(viewType){
            typeAD -> {
                val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_ad, parent, false)
                AdHolder(view)
            }
            typeLOADING -> {
                val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_loading, parent, false)
                LoadingHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_phrase, parent, false)
                PhraseHolder(view)
            }
        }

    interface OnClickShare{
        fun onClick(phrase: PhraseModel, view: View)
    }

    interface OnClickAuthor{
        fun onClick(phrase: PhraseModel)
    }

    interface OnClickCopy{
        fun onClick(phrase: PhraseModel)
    }

}