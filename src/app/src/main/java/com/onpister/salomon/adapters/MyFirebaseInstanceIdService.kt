package com.onpister.salomon.adapters

import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.firebase.messaging.FirebaseMessaging
import com.onpister.salomon.BuildConfig
import com.onpister.salomon.R
import com.onpister.salomon.models.UserPrefs

class MyFirebaseInstanceIdService : FirebaseInstanceIdService() {
    private val TAG = MyFirebaseInstanceIdService::class.java.getSimpleName()
    override fun onTokenRefresh() {
        try {
            val firebaseMessaging = FirebaseMessaging.getInstance()
            if (firebaseMessaging != null) {
                if(BuildConfig.DEBUG) {
                    firebaseMessaging.subscribeToTopic("dev")
                }
                var userPrefs = UserPrefs(this)
                if(userPrefs.notification) {
                    firebaseMessaging.subscribeToTopic("someone")
                    firebaseMessaging.subscribeToTopic(getString(R.string.pref_lang_default))
                }else{
                    firebaseMessaging.unsubscribeFromTopic("someone")
                    firebaseMessaging.unsubscribeFromTopic("en")
                    firebaseMessaging.unsubscribeFromTopic("pt")
                    firebaseMessaging.unsubscribeFromTopic("es")
                }
                firebaseMessaging.subscribeToTopic("everyone")

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        super.onTokenRefresh()
    }

}