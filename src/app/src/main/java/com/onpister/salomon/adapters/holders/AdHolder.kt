package com.onpister.salomon.adapters.holders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.onpister.salomon.Utils
import kotlinx.android.synthetic.main.item_ad.view.*

class AdHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bindView(adView: AdView, adUnitId: String){
        if(itemView.getTag() == null || itemView.getTag() != "loaded") {
            adView.adUnitId = adUnitId
            adView.adSize = AdSize.SMART_BANNER
            adView.setLayoutParams(ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT))
            adView.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                }
            }
            adView.loadAd(Utils.Ad.adRequest())
            itemView.adViewContainer.addView(adView)
            itemView.setTag("loaded")
        }
    }
}