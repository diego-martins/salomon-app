package com.onpister.salomon.adapters.holders

import android.support.v7.widget.RecyclerView
import android.view.View
import com.onpister.salomon.R
import com.onpister.salomon.adapters.MyAdapter
import com.onpister.salomon.models.PhraseModel
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_phrase.view.*

class PhraseHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bindView(model: PhraseModel){

        itemView.labelText.text = model.text
        itemView.labelAuthor.text = model.authorName
        itemView.labelAuthor.tag = model.authorId
        var resId = if (model.img > 0) model.img else R.drawable.img_phrase_default
        if(!model.imageUrl.isNullOrEmpty()){
            Picasso
                    .with(itemView.context)
                    .load(model.imageUrl)
                    .placeholder(R.drawable.img_author_default)
                    .error(R.drawable.img_author_default)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(itemView.imgBackground, object: Callback {
                        override fun onSuccess() {
                        }
                        override fun onError() {

                            Picasso
                                    .with(itemView.context)
                                    .load(model.imageUrl)
                                    .placeholder(resId)
                                    .error(resId)
                                    .into(itemView.imgBackground)
                        }
                    })
        }else {
            Picasso
                    .with(itemView.context)
                    .load(resId)
                    .into(itemView.imgBackground)
        }
    }

    fun bindEvent(
            model: PhraseModel,
            copyListener: MyAdapter.OnClickCopy?,
            shareListener: MyAdapter.OnClickShare?,
            authorListener: MyAdapter.OnClickAuthor?){

        if(copyListener != null){
            itemView.btnCopy.setOnClickListener {
                _ -> copyListener.onClick(model)
            }
        }

        if(shareListener != null){
            itemView.btnShare.setOnClickListener {
                _ -> shareListener.onClick(model, itemView)
            }
        }

        if(authorListener != null){
            itemView.viewAuthor.setOnClickListener {
                _ -> authorListener.onClick(model)
            }
        }

    }
}