package com.onpister.salomon.fragments

import android.os.Bundle
import android.preference.Preference
import android.preference.PreferenceFragment
import android.preference.SwitchPreference
import com.google.firebase.messaging.FirebaseMessaging
import com.onpister.salomon.BuildConfig
import com.onpister.salomon.R
import com.onpister.salomon.models.UserPrefs

class SettingsFragment: PreferenceFragment() {

    var userPrefs: UserPrefs? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.preferences)

        userPrefs = UserPrefs(activity)

        var notification = preferenceScreen.findPreference("notification") as SwitchPreference
        notification.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, any ->
            loadFCM(any as Boolean)
            true
        }

        var version = preferenceScreen.findPreference("version") as Preference
        version.title = getString(R.string.pref_version) + " " + BuildConfig.VERSION_NAME

    }

    fun loadFCM(enabled: Boolean){
        val firebaseMessaging = FirebaseMessaging.getInstance()
        if (firebaseMessaging != null) {
            if(BuildConfig.DEBUG) {
                firebaseMessaging.subscribeToTopic("dev")
            }
            if(enabled) {
                firebaseMessaging.subscribeToTopic("someone")
            }else{
                firebaseMessaging.unsubscribeFromTopic("someone")
            }
            firebaseMessaging.subscribeToTopic("everyone")
        }
    }
}