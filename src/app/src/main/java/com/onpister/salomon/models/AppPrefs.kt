package com.onpister.salomon.models

import android.content.Context
import android.content.SharedPreferences

class AppPrefs(context: Context){

    val PREFS_FILENAME = "com.onpister.salomon.app_prefs"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)

    /*
    var showedVoiceTip: Boolean
        get() = prefs.getBoolean("showed_voice_tip", false)
        set(value) = prefs.edit().putBoolean("showed_voice_tip", value).apply()
    */
}