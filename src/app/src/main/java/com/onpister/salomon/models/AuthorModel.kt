package com.onpister.salomon.models

import java.util.*

open class AuthorModel (
        var id: String = "",
        var nameEn: String = "",
        var namePt: String = "",
        var nameEs: String = "",
        var aboutEn: String = "",
        var aboutPt: String = "",
        var aboutEs: String = "",
        var imageUrl: String = "",
        var timestamp: Long = 0L,
        var createdAt: Date? = null,
        var modifiedAt: Date? = null,
        var enabled: Boolean = false
){}