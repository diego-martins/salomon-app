package com.onpister.salomon.models

import com.google.firebase.firestore.Exclude
import com.onpister.salomon.interfaces.IListItem
import java.util.*

open class PhraseModel (
        var id: String = "",
        var authorId: String = "",
        var authorName: String = "",
        var text: String = "",
        var timestamp: Long = 0L,
        var createdAt: Date? = null,
        var modifiedAt: Date? = null,
        var lang: String = "",
        var enabled: Boolean = false,
        var imageUrl: String = "",
        @get:Exclude
        var img: Int = 0
): IListItem