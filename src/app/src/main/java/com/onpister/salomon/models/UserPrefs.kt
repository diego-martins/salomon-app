package com.onpister.salomon.models

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class UserPrefs(var context: Context) {

    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    var notification: Boolean
        get() = prefs.getBoolean("notification", true)
        set(value) = prefs.edit().putBoolean("notification", value).apply()

    var lastPhraseAt: Long
        get() = prefs.getLong("lastPhraseAt", -1L)
        set(value) = prefs.edit().putLong("lastPhraseAt", value).apply()

}